package com.mitocode.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.model.SignosVitales;;

public interface ISignosVitalesService {

	int registrar(SignosVitales signosVitales);

	int modificar(SignosVitales signosVitales);

	void eliminar(int idSignosVitales);

	SignosVitales listarId(int idSignosVitales);

	List<SignosVitales> listar();
	
	Page<SignosVitales> listAllByPage(Pageable pageable);
}
