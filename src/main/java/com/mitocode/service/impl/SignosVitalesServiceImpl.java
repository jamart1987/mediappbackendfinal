package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.dao.ISignosVitalesDAO;
import com.mitocode.model.SignosVitales;
import com.mitocode.service.ISignosVitalesService;

@Service
public class SignosVitalesServiceImpl implements ISignosVitalesService {

	@Autowired
	private ISignosVitalesDAO dao;
	
	@Override
	public int registrar(SignosVitales signosVitales) {
		int rpta = 0;
		rpta = dao.save(signosVitales) != null ? signosVitales.getIdSignosVitales() : 0;
		return rpta > 0 ? 1 : 0;
	}

	@Override
	public int modificar(SignosVitales signosVitales) {
		int rpta = 0;
		rpta = dao.save(signosVitales) != null ? signosVitales.getIdSignosVitales() : 0;
		return rpta > 0 ? 1 : 0;
	}

	@Override
	public void eliminar(int idSignosVitales) {
		dao.delete(idSignosVitales);

	}

	@Override
	public SignosVitales listarId(int idSignosVitales) {
		return dao.findOne(idSignosVitales);
	}

	@Override
	public List<SignosVitales> listar() {
		return dao.findAll();
	}

	@Override
	public Page<SignosVitales> listAllByPage(Pageable pageable) {
		return dao.findAll(pageable);
	}

}
