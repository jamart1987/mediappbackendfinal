package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.SignosVitales;
import com.mitocode.service.ISignosVitalesService;

@RestController
@RequestMapping("/signos-vitales")
public class SignosVitalesController {
	
	@Autowired
	private ISignosVitalesService service;
	
	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<SignosVitales>> listar(){
		List<SignosVitales> SignosVitales = new ArrayList<>();
		try {
			SignosVitales = service.listar();
		}catch(Exception e) {
			return new ResponseEntity<List<SignosVitales>>(SignosVitales, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<SignosVitales>>(SignosVitales, HttpStatus.OK);
	}
	
	@GetMapping(value = "/listarPageable", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<SignosVitales>> listar(Pageable pageable){
		Page<SignosVitales> SignosVitales = null;
		try {
			SignosVitales = service.listAllByPage(pageable);
		}catch(Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<Page<SignosVitales>>(SignosVitales, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Page<SignosVitales>>(SignosVitales, HttpStatus.OK);
	}
	
	@GetMapping(value = "/listar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SignosVitales> listarId(@PathVariable("id") Integer id){
		SignosVitales SignosVitales = new SignosVitales();
		try {
			SignosVitales = service.listarId(id);
		}catch(Exception e) {
			return new ResponseEntity<SignosVitales>(SignosVitales, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<SignosVitales>(SignosVitales, HttpStatus.OK);
	}
	
	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrar(@RequestBody SignosVitales SignosVitales) {
		int rpta = 0;
		try {
			rpta = service.registrar(SignosVitales);			
		} catch (Exception e) {
			return new ResponseEntity<Integer>(rpta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(rpta, HttpStatus.OK);
	}
	
	@PutMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> actualizar(@RequestBody SignosVitales SignosVitales) {
		int rpta = 0;
		try {
			rpta = service.modificar(SignosVitales);			
		} catch (Exception e) {
			return new ResponseEntity<Integer>(rpta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Integer>(rpta, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminar(@PathVariable Integer id) {
		int resultado = 0;
		try {
			service.eliminar(id);
			resultado = 1;
		} catch (Exception e) {
			resultado = 0;
		}

		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}

}
